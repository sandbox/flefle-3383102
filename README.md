CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

Commerce Exchanger plugin for Narodna banka slovenska (NBS)
The base currency for exchange rates is NBS.


REQUIREMENTS
------------

This module requires Commerce Exchanger, and enabled EUR as currency.


CONFIGURATION
--------------

    1. Navigate to Administration > Extend and enable the Commerce Exchanger
       module.
    2. Navigate to Home > Administration > Commerce > Configuration
                   > Exchange rates.
