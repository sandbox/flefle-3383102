<?php

namespace Drupal\commerce_exchanger_nbs_eur\Plugin\Commerce\ExchangerProvider;

use Drupal\commerce_exchanger\Plugin\Commerce\ExchangerProvider\ExchangerProviderRemoteBase;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\NestedArray;

/**
 * Provides the Slovak national bank exchange rates (EUR).
 *
 * @CommerceExchangerProvider(
 *   id = "nbs_eur",
 *   label = "€ Narodna Banka Slovenska (NBS)",
 *   display_label = "Slovenska narodna banka",
 *   historical_rates = TRUE,
 *   base_currency = "EUR",
 *   refresh_once = TRUE,
 * )
 */

class NbsEurExchanger extends ExchangerProviderRemoteBase {

  /**
   * {@inheritdoc}
   */
  public function apiUrl() {
    return 'https://nbs.sk/export/en/exchange-rate/latest/xml';
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteData($base_currency = NULL) {
    $data = [];
    $request = $this->apiClient([]);
    if ($request) {
      try {
        $xml = simplexml_load_string($request);
      }
      catch (\Exception $e) {
        $this->logger->error($e->getMessage());
      }
      $json = json_encode($xml);
      $data = $this->parseCurrencyData(Json::decode($json, TRUE));
    }
    return $data;
  }

  private function parseCurrencyData($rawArr) {
    $returnRates = [];
    $mainArr = NestedArray::getValue($rawArr, ['Cube', 'Cube', 'Cube']);
    if ($mainArr) {
      foreach ($mainArr as $rawEntry) {
        $returnRates[$rawEntry['@attributes']['currency']] = str_replace(',', '.', $rawEntry['@attributes']['rate']);
      }
    }

    return $returnRates;
  }
}
